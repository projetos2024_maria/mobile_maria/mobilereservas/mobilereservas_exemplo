import React, { useState } from "react";
import { View, Button } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const DateTimePicker =({type, buttonTitle, dateKey, setSchedule})=>{ //definindo os parâmetros da const
    const [datePickerVisible,setDatePickerVisible] =useState(false);
   
    const showDatePicker = () =>{
        setDatePickerVisible(true);
    }//fim do showDatePicker

    const hideDatePicker = () => {
        setDatePickerVisible(false);
    } //fim do hideDatePicker

    const hadleConfirm = (date) => {
      if(type === "time"){
        //Lógica para extrair hora e minuto
     
        const hour = date.getHours(); 
        const minute = date.getMinutes();

        //Lógica para montar hora e minuto no formato desejado
        const formattedTime = `${hour}:${minute}`;

        //Atualiza o estado da reserva com HH:mm formata
        setSchedule((prevState)=>({
            ...prevState,
            [dateKey]:formattedTime,
            }));

    } // fim do if
      else{
        const formattedDate = date.toISOString().split('T')[0];
        //Atualizar Schedule
        setSchedule((prevState)=>({
        ...prevState,
        [dateKey]:formattedDate,
        }));
      }
      hideDatePicker();
    };

    return(
    <View>
       <Button title={buttonTitle} onPress={showDatePicker} color='#fff'/> 
       <DateTimePickerModal
       isVisible={datePickerVisible}
       mode={type}
       locale="pt_BR"
       onConfirm={hadleConfirm}
       onCancel={hideDatePicker}
       //Estilo Opcional
       pickerComponentStyleIOS={{backgroundColor: "#fff"}}
       textColor="#000"
       />
    </View>
  ); // fim do return
}; // fim const DateTimePicker
export default DateTimePicker;